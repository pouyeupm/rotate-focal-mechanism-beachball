#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 27 21:06:36 2019
@author: pouye
"""

import numpy as np
def rotate(X, theta, axis='x'):
  '''Rotate multidimensional array `X` `theta` degrees around axis `axis`'''
  c, s = np.cos(theta), np.sin(theta)
  if axis == 'x': return np.dot(np.array([
    [1.,  0,  0],
    [0 ,  c, -s],
    [0 ,  s,  c]
  ]),X)
  elif axis == 'y': return np.dot(np.array([
    [c,  0,  s],
    [0,  1,   0],
    [-s,  0,   c]
  ]),X)
  elif axis == 'z': return np.dot(np.array([
    [c, -s,  0 ],
    [s,  c,  0 ],
    [0,  0,  1.],
  ]),X)
    
def beachballview(strike,dip,rake,az,elev):
    Azimuth=-1*(az+90) 
    Elevation=elev-90
    
    xs=np.cos(np.radians(450-strike))
    ys=np.sin(np.radians(450-strike))
    zs=0
    strikevector=[xs,ys,zs]
    
    xyd=np.cos(np.radians(dip))
    xd=np.cos(np.radians(450-strike-90))*xyd
    yd=np.sin(np.radians(450-strike-90))*xyd
    zd=-1*np.sin(np.radians(dip))
    dipvector=[xd,yd,zd]
    
    n1=np.cross(dipvector,strikevector)
    n1=n1/np.linalg.norm(n1)
    
    rs=np.cos(np.radians(rake))
    rd=-1*np.sin(np.radians(rake))
    zr=-1*np.sin(np.radians(dip))*rd
    xyr1=np.cos(np.radians(dip))*rd
    xr1=np.cos(np.radians(450-strike-90))*xyr1
    yr1=np.sin(np.radians(450-strike-90))*xyr1
    xr2=np.cos(np.radians(450-strike))*rs
    yr2=np.sin(np.radians(450-strike))*rs
    n2=[xr1+xr2,yr1+yr2,zr]
    n2=n2/np.linalg.norm(n2)
    
    n1_=rotate(n1,np.radians(Azimuth),axis='z')
    n1_=n1_/np.linalg.norm(n1_)
    n1_new=rotate(n1_,np.radians(Elevation),axis='x')
    n1_new=n1_new/np.linalg.norm(n1_new)
    
    n2_=rotate(n2,np.radians(Azimuth),axis='z')
    n2_=n2_/np.linalg.norm(n2_)
    n2_new=rotate(n2_,np.radians(Elevation),axis='x')
    n2_new=n2_new/np.linalg.norm(n2_new)
    
    newstrike=np.cross([0,0,1],n1_new)
    newstrike=newstrike/np.linalg.norm(newstrike)
    a=newstrike[0]
    newstrike_d=np.degrees(np.arccos(np.dot(newstrike,[0,1,0])))
    if a<0:
        newstrike_d=360-newstrike_d 
        
    newdip=np.cross(newstrike,n1_new)
    newdip_d=np.degrees(np.arctan(-1*newdip[2]/np.sqrt(newdip[0]*newdip[0]+newdip[1]*newdip[1])))
    newrake_d=np.degrees(np.arccos(np.dot(n2_new,newstrike))) 
    if round(n2_new[2],5)<0:
        newrake_d=-1*newrake_d
        
    if n1_new[2]<0:
        newstrike_d=newstrike_d-180
        newrake_d=-1*newrake_d
        
    return newstrike_d,newdip_d,newrake_d
    
    