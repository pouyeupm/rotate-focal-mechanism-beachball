#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 27 23:39:21 2019

@author: pouye
"""

import matplotlib.pyplot as plt
from obspy.imaging.beachball import beachball
from beachballRotation import beachballview   

strike=28
dip=55
rake=-94

##-------> if az=-90 : look from a normal view with North at up and if you want to look from the South-East then az=-45
az=-20    
##-------> if elev=90 : look from top, like you are the GOD! and if elev=0 : look from surafce, like you are in the beach :D
elev=0 

newstrike,newdip,ranewke=beachballview(strike,dip,rake,az,elev)
rotated = [newstrike,newdip,ranewke]
beachrotated = beachball(rotated,facecolor='k')
